## Currency Converter

Solution for the test task provided by Revolut Company

---

## Getting Started

For comfortable work preferred XCode 10.1+

---

## Built With

* XCode 10.1
* Swift 4.2

---

## Tests

Includes Unit tests

---