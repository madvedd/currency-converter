//
//  CurrenciesViewController.swift
//  currencies
//
//  Created by madvedd on 29/01/2019.
//  Copyright © 2019 madvedd'Co. All rights reserved.
//

import UIKit

class CurrenciesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var connectionErrorHeightConstraint: NSLayoutConstraint!
    
    let connectionErrorDefaultHeight: CGFloat = 40
    let cellHeight: CGFloat = 70
    
    var viewModel: CurrenciesViewModel!
    
    let topCellIndex = IndexPath(item: 0, section: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideConnectionError()
        self.startActivity()
        self.setupTableView()
        self.viewModel = CurrenciesViewModel(withDelegate: self)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    private func setupTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = self.cellHeight
        self.tableView.estimatedRowHeight = self.cellHeight
    }
    
    private func startActivity() {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }
    
    private func stopActivity() {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }
    
    private func hideConnectionError() {
        self.connectionErrorHeightConstraint.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    private func showConnectionError() {
        self.connectionErrorHeightConstraint.constant = self.connectionErrorDefaultHeight
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}

extension CurrenciesViewController: CurrenciesViewModelDelegate {
    func initialisationCellsViewModelsEnded() {
        // reload table after initialisation cellsViewModels
        self.tableView.reloadData()
        self.stopActivity()
    }
    
    func baseCurrencyCellViewModelChanged(withIndex index: IndexPath) {
        // move new base currency to the top and scroll to it and after that call firstResponder on the base cell cell
        CATransaction.begin()
        CATransaction.setCompletionBlock({ [weak self] in
            if  let strongSelf = self,
                let cell = strongSelf.tableView.cellForRow(at: strongSelf.topCellIndex) as? CurrencyCell {
                cell.callFirstResponder()
            }
        })
        self.tableView.beginUpdates()
        self.tableView.moveRow(at: index, to: self.topCellIndex)
        self.tableView.endUpdates()
        self.tableView.scrollToRow(at: self.topCellIndex, at: UITableView.ScrollPosition.top, animated: true)
        CATransaction.commit()
    }
    
    func currenciesViewModelsUpdated() {
        // Reload all atems except first one after cells viewModels were updated
        let rowsCount = self.tableView.numberOfRows(inSection: 0)
        var indexes = [IndexPath]()
        for number in 1..<rowsCount {
            indexes.append(IndexPath(row: number, section: 0))
        }        
        self.tableView.reloadRows(at: indexes, with: .none)
        
    }
    
    func requestCurrenciesError(_ error: Error?) {
        self.showConnectionError()
    }
    
    func currenciesSsuccessfullyFetched() {
        self.hideConnectionError()
    }
}

extension CurrenciesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.currecySelected(withIndex: indexPath)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        // hide keyboard on scroll
        if let cell = self.tableView.cellForRow(at: self.topCellIndex) as? CurrencyCell {
            cell.currencyTextField.resignFirstResponder()
        }
    }
}

extension CurrenciesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.currenciesCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CurrencyCell.identifier,
                                                       for: indexPath) as? CurrencyCell else {
            fatalError("Can't dequeue a cell with identifier: \(CurrencyCell.identifier)")
        }
        cell.selectionStyle = .none        
        if let currencyViewModel = self.viewModel.currencyCellViewModel(forIndex: indexPath) {
            cell.configure(withViewModel: currencyViewModel, andDelegate: self)
        }
        return cell
    }
}

extension CurrenciesViewController: CurrencyCellDelegate {
    func textFieldUpdated(withText: String) {
        self.viewModel.baseCurrencyRateChanged(withText: withText)
    }
}
