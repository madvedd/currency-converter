//
//  CurrencyModel.swift
//  currencies
//
//  Created by Vitaliy Medvedev on 31.01.19.
//  Copyright © 2019 madvedd'Co. All rights reserved.
//

import UIKit

struct CurrencyModel {
    let name: String
    let rate: Double
}
