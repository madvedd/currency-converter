//
//  CurenciesResponceData.swift
//  currencies
//
//  Created by Vitaliy Medvedev on 06.02.19.
//  Copyright © 2019 madvedd'Co. All rights reserved.
//

import UIKit

/// struct for decode json responce into it
struct CurenciesResponceData: Codable {
    var base: String
    var date: String
    var rates: [String: Double]
}
