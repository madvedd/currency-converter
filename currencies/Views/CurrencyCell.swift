//
//  CurrencyCell.swift
//  currencies
//
//  Created by Vitaliy Medvedev on 30.01.19.
//  Copyright © 2019 madvedd'Co. All rights reserved.
//

import UIKit

protocol CurrencyCellDelegate: NSObjectProtocol {
    func textFieldUpdated(withText: String)
}

final class CurrencyCell: UITableViewCell {

    static let identifier = "CurrencyTableViewCell"
    
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var currencyTextField: UITextField!
    
    weak var delegate: CurrencyCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setBottomBorder()
        self.currencyTextField.delegate = self
        self.currencyTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    /// Configurate a cell with a ViewModel and delegate
    ///
    /// - Parameter viewModel: view model for cell
    /// - Parameter callback: success callback with CurenciesResponceData
    /// - Parameter delegate: delegate that handle cell changes
    public func configure(withViewModel viewModel: CurrencyCellViewModel, andDelegate delegate: CurrencyCellDelegate) {
        self.delegate = delegate
        self.flagImageView.image = viewModel.getImage()
        self.currencyLabel.text = viewModel.name
        self.descriptionLabel.text = viewModel.getDescription()
        self.currencyTextField.text = viewModel.getRateText()
    }
    
    ///call first responder on text field
    public func callFirstResponder() {
        self.currencyTextField.isUserInteractionEnabled = true
        self.currencyTextField.becomeFirstResponder()
    }
    
    ///set bottom line under text field
    private func setBottomBorder() {
        self.currencyTextField.borderStyle = .none
        self.currencyTextField.layer.backgroundColor = UIColor.white.cgColor
        self.currencyTextField.layer.masksToBounds = false
        self.currencyTextField.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.currencyTextField.layer.shadowOpacity = 1.0
        self.currencyTextField.layer.shadowRadius = 0.0
        self.setBottomBorderColor()
    }
    
    ///set collor for bottom line under text field
    private func setBottomBorderColor(isSelected: Bool = false) {
        self.currencyTextField.layer.shadowColor = isSelected ? UIColor.blue.cgColor : UIColor.lightGray.cgColor
    }
    
    ///get text field changes ans send them to the delegate
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.delegate?.textFieldUpdated(withText: textField.text ?? "")
    }
}

extension CurrencyCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        //allow enter in text field only one separator and decimal digits
        var allowedCharacters = CharacterSet.decimalDigits
        if let separator = NSLocale.current.decimalSeparator {
            allowedCharacters.insert(charactersIn: "\(separator)")
            if let currentText = textField.text {
                //after firs "0" should be only separator
                if currentText == "0" && string != separator{
                    return false
                }
                if string == separator {
                    let countSeparators = currentText.components(separatedBy: separator).count - 1
                    if countSeparators > 0 {
                        return false
                    }
                }
            }
        }
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {        
        self.setBottomBorderColor(isSelected: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.setBottomBorderColor(isSelected: false)
        self.currencyTextField.isUserInteractionEnabled = false
    }
}
