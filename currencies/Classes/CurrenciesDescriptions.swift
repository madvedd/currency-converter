//
//  CurrenciesDescriptions.swift
//  currencies
//
//  Created by Vitaliy Medvedev on 06.02.19.
//  Copyright © 2019 madvedd'Co. All rights reserved.
//

import UIKit


class CurrenciesDescriptions {
    
    /// static dictionary ["currency name": "description"]
    static let descriptions: [String:String] = [
        "EUR" : "Euro" ,
        "AUD" : "Australian Dollar" ,
        "BGN" : "Bulgarian Lev" ,
        "BRL" : "Brazilian Real" ,
        "CAD" : "Canadian Dollar" ,
        "CHF" : "Swiss Franc" ,
        "CNY" : "Renminbi" ,
        "CZK" : "Czech Koruna" ,
        "DKK" : "Danish Krone" ,
        "GBP" : "British Pound" ,
        "HKD" : "Hong Kong Dollar" ,
        "HRK" : "Croatian Kuna" ,
        "HUF" : "Hungarian Forint" ,
        "IDR" : "Indonesian Rupiah" ,
        "ILS" : "Israeli Shekel" ,
        "INR" : "Indian Rupee" ,
        "ISK" : "Icelandic Króna" ,
        "JPY" : "Japanese Yen" ,
        "KRW" : "South Korean Won" ,
        "MXN" : "Mexican Peso" ,
        "MYR" : "Malaysian Ringgit" ,
        "NOK" : "Norwegian Krone" ,
        "NZD" : "New Zealand Dollar" ,
        "PHP" : "Philippine Peso" ,
        "PLN" : "Polish Złoty" ,
        "RON" : "Romanian Leu" ,
        "RUB" : "Russian Ruble" ,
        "SEK" : "Swedish Krona" ,
        "SGD" : "Singapore Dollar" ,
        "THB" : "Thai Baht" ,
        "TRY" : "Turkish Lira" ,
        "USD" : "US Dollar" ,
        "ZAR" : "South African Rand"
    ]
}
