//
//  CurrenciesDataHandler.swift
//  currencies
//
//  Created by Vitaliy Medvedev on 04.02.19.
//  Copyright © 2019 madvedd'Co. All rights reserved.
//

import UIKit

class CurrenciesDataHandler {
    let currenciesUrl = "https://revolut.duckdns.org/latest?base="
    
    /// Request Currencies rates for base currency
    ///
    /// - Parameter currencyName: name of base currency
    /// - Parameter callback: success callback with CurenciesResponceData
    /// - Parameter errorCallback: error callback with Error
    func getCurrencies(withCurrencyBaseName currencyName: String,
                       callback: @escaping (CurenciesResponceData) -> Void,
                       errorCallback: @escaping (Error?) -> Void) {
        
        guard let url = URL(string: "\(currenciesUrl)\(currencyName)") else {
            return
        }
        let task = URLSession.shared.dataTask(with: url) { (data, responce, error) in
            guard let dataResponse = data,
                error == nil else {
                    print(error?.localizedDescription ?? "Response Error")
                    DispatchQueue.main.async {
                        errorCallback(error)
                    }
                    return
            }
            do {
                let decoder = JSONDecoder()
                let curenciesResponce = try decoder.decode(CurenciesResponceData.self, from: dataResponse)
                DispatchQueue.main.async {
                    callback(curenciesResponce)
                }
            } catch let parsingError {
                DispatchQueue.main.async {
                    print("Error", parsingError)
                    errorCallback(parsingError)
                }
            }
        }
        task.resume()
    }    
}
