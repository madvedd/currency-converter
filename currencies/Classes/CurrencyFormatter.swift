//
//  CurrencyFormatter.swift
//  currencies
//
//  Created by Vitaliy Medvedev on 07.02.19.
//  Copyright © 2019 madvedd'Co. All rights reserved.
//

import Foundation

class CurrencyFormatter: NumberFormatter {
    override init() {
        super.init()
        self.setFormatter()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setFormatter()
    }
    
    private func setFormatter() {
        self.numberStyle = .decimal
        self.locale = Locale.current
        self.roundingIncrement = 0.01
        self.usesGroupingSeparator = false
    }
}
