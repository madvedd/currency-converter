//
//  CurrencyCellViewModel.swift
//  currencies
//
//  Created by Vitaliy Medvedev on 31.01.19.
//  Copyright © 2019 madvedd'Co. All rights reserved.
//

import UIKit

final class CurrencyCellViewModel {
    
    var currencyModel: CurrencyModel
    
    let currencyFormatter = CurrencyFormatter()
    
    /// Currency name
    public var name: String {
        return self.currencyModel.name
    }
    
    /// Currency rate
    public var rate: Double {
        return self.currencyModel.rate
    }
    
    init(name: String, rate: Double) {        
        self.currencyModel = CurrencyModel(name: name, rate: rate)
    }        
}

extension CurrencyCellViewModel {
    
    /// set new rate for currency it's nessesary for changing base currency rate
    ///
    /// - Parameter newRate
    public func setNewRate(_ newRate: Double) {
        self.currencyModel = CurrencyModel(name: self.name, rate: newRate)
    }
    
    /// get description for currency or nil
    public func getDescription() -> String? {
        return CurrenciesDescriptions.descriptions[self.name.uppercased()]
    }
    
    /// get image for currency
    public func getImage() -> UIImage? {
        if let image = UIImage(named: "flag_\(self.name.lowercased())") {
            return image
        }
        return UIImage(named: "flag_err")
    }
    
    /// get formatted text of currency rate
    public func getRateText() -> String {
        if self.rate == 0 {
            return ""
        }
        return self.currencyFormatter.string(from: NSNumber(value: self.rate)) ?? ""
    }
}
