//
//  CurrenciesViewModel.swift
//  currencies
//
//  Created by Vitaliy Medvedev on 31.01.19.
//  Copyright © 2019 madvedd'Co. All rights reserved.
//

import UIKit

protocol CurrenciesViewModelDelegate: NSObjectProtocol {
    /// calls after viewModels for currencies cells were initialised
    func initialisationCellsViewModelsEnded()
    
    /// calls if new a currency was chosen to be the base one
    func baseCurrencyCellViewModelChanged(withIndex index: IndexPath)
    
    /// calls if view models for currencies cells were updated
    func currenciesViewModelsUpdated()
    
    /// calls if currency request failed
    func requestCurrenciesError(_ error: Error?)
    /// calls if currency request was successful
    func currenciesSsuccessfullyFetched()
}

final class CurrenciesViewModel {
    
    private let initialBaseCurrency = CurrencyCellViewModel(name: "EUR", rate: 1)
    
    weak var delegate: CurrenciesViewModelDelegate!
    
    var currenciesUpdateTimer: Timer?
    
    private let dataHandler: CurrenciesDataHandler = CurrenciesDataHandler()
    
    private var currencyViewModels: [CurrencyCellViewModel] = [CurrencyCellViewModel]()
    
    let currencyFormatter = CurrencyFormatter()
    
    private init () {}
    
    /// Init method starts initialisation right after it was called
    ///
    /// - Parameter delegate: delegate that will handle view model changes
    init(withDelegate delegate: CurrenciesViewModelDelegate) {
        self.delegate = delegate
        self.fetchCurrencies(forBaseCurrency: initialBaseCurrency)
    }
    
    /// tell viewModel that base currency rate was changed with a new rate text
    ///
    /// - Parameter newRateText: new rate as a string
    public func baseCurrencyRateChanged(withText newRateText: String) {
        guard let baseCurrency = self.currencyViewModels.first else {
            return
        }
        let newRateNumber = self.currencyFormatter.number(from: newRateText)
        let newRate = newRateNumber?.doubleValue ?? 0
        baseCurrency.setNewRate(newRate)
        self.fetchCurrencies(forBaseCurrency: baseCurrency)
    }
    
    /// tell viewModel that new base currency was selected
    ///
    /// - Parameter index: index of a new base currency
    public func currecySelected(withIndex index: IndexPath) {
        guard self.currenciesCount() > index.item else {
            return
        }
        self.stopCurrenciesUpdates()
        let currencyToMove = self.currencyViewModels.remove(at: index.item)
        self.currencyViewModels.insert(currencyToMove, at: 0)
        self.delegate.baseCurrencyCellViewModelChanged(withIndex: index)
        self.scheduleCurrenciesUpdates()
    }
    
    /// how many currencies cells viewModels we have
    public func currenciesCount() -> Int {
        return self.currencyViewModels.count
    }
    
    /// get currencyCellViewModel for index
    ///
    /// - Parameter index
    public func currencyCellViewModel(forIndex index: IndexPath) -> CurrencyCellViewModel? {
        let item = index.item
        if self.currenciesCount() > item {
            return currencyViewModels[index.item]
        }
        return nil
    }
    
    
    @objc private func callCurrenciesUpdate() {
        let baseCurrency = self.currencyViewModels.first ?? self.initialBaseCurrency
        self.fetchCurrencies(forBaseCurrency: baseCurrency)
    }
    
    /// schedule next update for currencies
    private func scheduleCurrenciesUpdates() {
        self.currenciesUpdateTimer = Timer.scheduledTimer(timeInterval: 1,
                                                          target: self,
                                                          selector: #selector(self.callCurrenciesUpdate),
                                                          userInfo: nil,
                                                          repeats: false)        
    }
    private func stopCurrenciesUpdates() {
        if let timer = self.currenciesUpdateTimer {
            timer.invalidate()
            self.currenciesUpdateTimer = nil
        }
    }
    
    /// request currencies rates
    private func fetchCurrencies(forBaseCurrency baseCurrency: CurrencyCellViewModel) {
        self.stopCurrenciesUpdates()
        self.dataHandler.getCurrencies(withCurrencyBaseName: baseCurrency.name,
                                       callback: { [weak self] (curenciesResponce) in
                                        self?.delegate?.currenciesSsuccessfullyFetched()
                                        guard  let strongSelf = self,
                                            curenciesResponce.base == baseCurrency.name else {
                                                return
                                        }
                                        if strongSelf.currencyViewModels.count == 0 {
                                            strongSelf.createCurrencies(fromResponce: curenciesResponce, baseCurrency: baseCurrency)
                                        } else {
                                            strongSelf.updateRatesForCurrencies(fromResponce: curenciesResponce, baseCurrency: baseCurrency)
                                        }
                                        strongSelf.scheduleCurrenciesUpdates()
        }) { [weak self] (error) in
            self?.delegate?.requestCurrenciesError(error)
            self?.scheduleCurrenciesUpdates()
        }
        
    }
    
    /// init currencyViewModels
    private func createCurrencies(fromResponce curenciesResponce: CurenciesResponceData, baseCurrency: CurrencyCellViewModel) {
        var currencyViewModels = [CurrencyCellViewModel]()
        currencyViewModels.append(baseCurrency)
        for (currenyName, rate) in curenciesResponce.rates {
            let newRate = rate * baseCurrency.rate
            currencyViewModels.append(CurrencyCellViewModel(name: currenyName, rate: newRate))
        }
        self.currencyViewModels = currencyViewModels
        self.delegate.initialisationCellsViewModelsEnded()
    }
    
    /// update currencyViewModels
    private func updateRatesForCurrencies(fromResponce curenciesResponce: CurenciesResponceData, baseCurrency: CurrencyCellViewModel) {
        for viewModel in self.currencyViewModels {
            if var newRate = curenciesResponce.rates[viewModel.name] {
                newRate = newRate * baseCurrency.rate
                viewModel.setNewRate(newRate)
            }
        }
        self.delegate.currenciesViewModelsUpdated()
    }
}
