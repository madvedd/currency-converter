//
//  CurrenciesViewModelTests.swift
//  currenciesTests
//
//  Created by Vitaliy Medvedev on 06.02.19.
//  Copyright © 2019 madvedd'Co. All rights reserved.
//

import XCTest

class CurrenciesViewModelTests: XCTestCase {
    
    let testTimeout: TimeInterval = 5.0
    
    let selectedIndexPath = IndexPath(item: 5, section: 0)
    
    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testInitCellsViewModels() {
        let callReloadTableExpectation = self.expectation(description: "Expect that reload Table Will be called")
        let delegate = CurrencieslViewModelTestDelegate()
        let viewModel = CurrenciesViewModel(withDelegate: delegate)
        delegate.initialisationEndedCallback = {
            XCTAssertTrue(viewModel.currenciesCount() > 0)
            XCTAssertTrue(viewModel.currencyCellViewModel(forIndex: self.selectedIndexPath) != nil)
            callReloadTableExpectation.fulfill()
        }
        self.waitForExpectations(timeout: self.testTimeout) { (error) in
            if let error = error {
                XCTFail("Expectation failed with error: \(error.localizedDescription)")
            }
        }
    }
    
    func testCurrecySelected() {
        let callSetToTopCellExpectation = self.expectation(description: "Expect that setToTopCell Will be called")
        let delegate = CurrencieslViewModelTestDelegate()
        let viewModel = CurrenciesViewModel(withDelegate: delegate)
        var selectedCellViewModel: CurrencyCellViewModel!
        delegate.initialisationEndedCallback = {
            selectedCellViewModel = viewModel.currencyCellViewModel(forIndex: self.selectedIndexPath)
            viewModel.currecySelected(withIndex: self.selectedIndexPath)
        }
        delegate.baseCurrencyChangedCallback = { (index) in
            guard let baseViewModel = viewModel.currencyCellViewModel(forIndex: IndexPath(item: 0, section: 0)) else {
                XCTFail("Base cellViewModel can't be nill")
                return
            }
            XCTAssert(selectedCellViewModel.name == baseViewModel.name, "cellViewModel should be first now")
            XCTAssertTrue(index == self.selectedIndexPath)
            callSetToTopCellExpectation.fulfill()
        }
        self.waitForExpectations(timeout: self.testTimeout) { (error) in
            if let error = error {
                XCTFail("Expectation failed with error: \(error.localizedDescription)")
            }
        }
    }
    
    func testBaseCurrencyRateChanged() {
        let callUpdateUnderTopCellsExpectation = self.expectation(description: "Expect that currenciesUpdatedCallback Will be called")
        let delegate = CurrencieslViewModelTestDelegate()
        let viewModel = CurrenciesViewModel(withDelegate: delegate)
        var selectedCellViewModelRate: Double!
        let newBaseRate: Double = 100.34
        guard let decimalSeparator = Locale.current.decimalSeparator else {
            XCTFail("decimalSeparator can't be nill")
            return
        }
        let newBaseRateText = "100\(decimalSeparator)34"
        delegate.initialisationEndedCallback = {
            guard let cellViewModel = viewModel.currencyCellViewModel(forIndex: self.selectedIndexPath) else {
                XCTFail("Base cellViewModel can't be nill")
                return
            }
            selectedCellViewModelRate = cellViewModel.rate
            viewModel.baseCurrencyRateChanged(withText: newBaseRateText)
        }
        delegate.currenciesUpdatedCallback = {
            guard let baseCellViewModel = viewModel.currencyCellViewModel(forIndex: IndexPath(item: 0, section: 0)),
                let cellViewModel = viewModel.currencyCellViewModel(forIndex: self.selectedIndexPath) else {
                XCTFail("cellViewModel can't be nill")
                return
            }
            XCTAssertTrue(cellViewModel.rate > selectedCellViewModelRate)
            XCTAssertTrue(baseCellViewModel.rate == newBaseRate)
            callUpdateUnderTopCellsExpectation.fulfill()
        }
        self.waitForExpectations(timeout: self.testTimeout) { (error) in
            if let error = error {
                XCTFail("Expectation failed with error: \(error.localizedDescription)")
            }
        }
    }
    
    func testcurrenciesSsuccessfullyFetched(){
        let currenciesSsuccessfullyFetchedExpectation = self.expectation(description: "Expect that currenciesSsuccessfullyFetched Will be called")
        let delegate = CurrencieslViewModelTestDelegate()
        let viewModel = CurrenciesViewModel(withDelegate: delegate)
        delegate.currenciesSsuccessfullyFetchedCallback = {
            currenciesSsuccessfullyFetchedExpectation.fulfill()
        }
        self.waitForExpectations(timeout: self.testTimeout) { (error) in
            if let error = error {
                XCTFail("Expectation failed with error: \(error.localizedDescription)")
            }
        }
    }
    
}

class CurrencieslViewModelTestDelegate: NSObject, CurrenciesViewModelDelegate {
    
    var initialisationEndedCallback: (() -> Void)?
    var baseCurrencyChangedCallback: ((IndexPath) -> Void)?
    var currenciesUpdatedCallback: (() -> Void)?
    var requestCurrenciesErrorCallback: (() -> Void)?
    var currenciesSsuccessfullyFetchedCallback: (() -> Void)?
    
    func initialisationCellsViewModelsEnded() {
        self.initialisationEndedCallback?()
    }
    
    func baseCurrencyCellViewModelChanged(withIndex index: IndexPath) {
        self.baseCurrencyChangedCallback?(index)
    }
    
    func currenciesViewModelsUpdated() {
        self.currenciesUpdatedCallback?()
    }
    
    func requestCurrenciesError(_ error: Error?) {
        self.requestCurrenciesErrorCallback?()
    }
    
    func currenciesSsuccessfullyFetched() {
        self.currenciesSsuccessfullyFetchedCallback?()
    }
}
