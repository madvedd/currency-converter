//
//  CurrencyCellViewModelTests.swift
//  currenciesTests
//
//  Created by Vitaliy Medvedev on 06.02.19.
//  Copyright © 2019 madvedd'Co. All rights reserved.
//

import XCTest

class CurrencyCellViewModelTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testRateText() {
        let viewModel = CurrencyCellViewModel(name: "EUR", rate: 1.23434)
        if let separator = NSLocale.current.decimalSeparator {
            XCTAssertTrue(viewModel.getRateText() == "1\(separator)23")
        }
        viewModel.setNewRate(0)
        XCTAssertTrue(viewModel.getRateText() == "")
        viewModel.setNewRate(10.0)
        XCTAssertTrue(viewModel.getRateText() == "10")
    }
    
    func testImage() {
        var viewModel = CurrencyCellViewModel(name: "EUR", rate: 1)
        XCTAssertTrue(viewModel.getImage() != nil)
        viewModel = CurrencyCellViewModel(name: "LOL", rate: 1)
        XCTAssertTrue(viewModel.getImage() != nil)
    }
    
    func testDescription() {
        var viewModel = CurrencyCellViewModel(name: "EUR", rate: 1)
        XCTAssertTrue(viewModel.getDescription() != nil)
        viewModel = CurrencyCellViewModel(name: "LOL", rate: 1)
        XCTAssertTrue(viewModel.getDescription() == nil)
    }
}
