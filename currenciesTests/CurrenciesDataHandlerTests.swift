//
//  CurrenciesDataHandlerTests.swift
//  currenciesTests
//
//  Created by Vitaliy Medvedev on 06.02.19.
//  Copyright © 2019 madvedd'Co. All rights reserved.
//

import XCTest

class CurrenciesDataHandlerTests: XCTestCase {

    let dataHandler: CurrenciesDataHandler = CurrenciesDataHandler()
    let testTimeout: TimeInterval = 5.0
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFetchCurrencies() {
        let baseCurrencyName = "EUR"
        let fetchCurrenciesExpectation: XCTestExpectation = self.expectation(description: "")
        self.dataHandler.getCurrencies(withCurrencyBaseName: baseCurrencyName, callback: { (curenciesResponce) in
            XCTAssert(curenciesResponce.base == baseCurrencyName, "Currencies base name should be - \(baseCurrencyName)")
            XCTAssert(curenciesResponce.rates.count > 0, "Currencies count should be greater then 0")
            fetchCurrenciesExpectation.fulfill()
        }) { (error) in
            XCTFail("error block should not be called \(error?.localizedDescription ?? "")")
        }
        self.waitForExpectations(timeout: self.testTimeout) { (error) in
            if let error = error {
                XCTFail("Expectation failed with error: \(error.localizedDescription)")
            }
        }
    }
    
    func testFetchCurrenciesWithError() {
        let baseCurrencyName = "LOL"
        let fetchCurrenciesExpectation: XCTestExpectation = self.expectation(description: "")
        self.dataHandler.getCurrencies(withCurrencyBaseName: baseCurrencyName, callback: { (curenciesResponce) in
            XCTFail("success block should not be called")
        }) { (error) in
            print(error?.localizedDescription ?? " vrong fetch")
            fetchCurrenciesExpectation.fulfill()
        }
        self.waitForExpectations(timeout: self.testTimeout) { (error) in
            if let error = error {
                XCTFail("Expectation failed with error: \(error.localizedDescription)")
            }
        }
    }

}
